const express = require('express');
const app = express();
const PORT = 3000;

const responses = [
  [{
    "subject": "Anim fugiat non ex id irure incididunt sit deserunt aute officia adipisicing anim cupidatat.",
    "body": "Quis irure cillum minim et do. Sint minim non elit duis irure anim minim quis eu dolore laborum officia commodo. Et id cillum consequat ea qui Lorem ex irure ad eu. Ea in occaecat ad tempor mollit incididunt anim nostrud ex. Adipisicing reprehenderit non ut ex aute nisi. Sit fugiat reprehenderit culpa nisi consectetur excepteur. Voluptate qui laboris irure esse proident id quis mollit ad fugiat esse."},
  {
    "subject": "Laboris aliquip eiusmod amet cillum aliquip veniam occaecat nisi ipsum dolore ullamco deserunt eu.",
    "body": "Veniam aute veniam amet sint non incididunt Lorem amet nulla ea sunt. Adipisicing elit eu Lorem reprehenderit excepteur proident do duis cupidatat irure ea et sunt. Proident adipisicing ad sunt adipisicing consectetur ipsum laborum voluptate ut. Sit magna cillum mollit magna esse irure laboris reprehenderit minim eiusmod laborum. Proident ullamco sit fugiat irure sit consequat eiusmod tempor cillum elit minim."
  }],
  [
    {
      "subject": "Laboris aliquip eiusmod amet cillum aliquip veniam occaecat nisi ipsum dolore ullamco deserunt eu.",
      "body": "Veniam aute veniam amet sint non incididunt Lorem amet nulla ea sunt. Adipisicing elit eu Lorem reprehenderit excepteur proident do duis cupidatat irure ea et sunt. Proident adipisicing ad sunt adipisicing consectetur ipsum laborum voluptate ut. Sit magna cillum mollit magna esse irure laboris reprehenderit minim eiusmod laborum. Proident ullamco sit fugiat irure sit consequat eiusmod tempor cillum elit minim."
    },
    {
      "subject": "Anim fugiat non ex id irure incididunt sit deserunt aute officia adipisicing anim cupidatat.",
      "body": "Quis irure cillum minim et do. Sint minim non elit duis irure anim minim quis eu dolore laborum officia commodo. Et id cillum consequat ea qui Lorem ex irure ad eu. Ea in occaecat ad tempor mollit incididunt anim nostrud ex. Adipisicing reprehenderit non ut ex aute nisi. Sit fugiat reprehenderit culpa nisi consectetur excepteur. Voluptate qui laboris irure esse proident id quis mollit ad fugiat esse."
    }],
  {"error": true}
]

app.get('/', (req, res) => {
  res.send('Go to /random-campaign-email');
});

app.get('/random-campaign-email', (req, res) => {

  const randCampaignEmail = responses[Math.floor(Math.random()*responses.length)];
  if ("error" in randCampaignEmail) {
    res.status(500).send("Internal Server Error!")
    return
  }

  res.send(randCampaignEmail);
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
