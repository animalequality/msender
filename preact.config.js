import webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export default (config, env, helpers) => {
    config.output.filename = "[name].js";

    config.resolve.alias = Object.assign({}, config.resolve.alias, {
        'react': 'preact/compat',
        'react-dom': 'preact/compat'
    });
    config.plugins.push(
        new webpack.ProvidePlugin({
            Component: ['preact', 'Component'],
            React: ['preact-compat']
        })
    );

    // fix bug https://github.com/preactjs/preact-cli/issues/657
    config.plugins = config.plugins.filter(plugin => !(plugin instanceof MiniCssExtractPlugin));
    config.module.rules = config.module.rules.map(rule => {
        if (rule.use) {
            return {
                ...rule,
                use: rule.use.reduce((acc, loader) => {
                    loader !== MiniCssExtractPlugin.loader ? acc.push(loader) : acc.push(
                        'style-loader');

                    return acc;
                }, []),
            };
        }

        return rule;
    });

    if (env.production) {
        config.output.libraryTarget = "umd";
    }
};
