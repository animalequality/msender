import React, { Component } from 'react'

export function getPetitionUserData(local_storage_name = 'msender.userdata') {
  if (typeof(Storage) === 'undefined') {
    return null
  }
  let userData = localStorage.getItem(local_storage_name)
  if (!userData) {
    return null;
  }
  userData = JSON.parse(userData) || null
  return userData
}

export default function(WrappedComponent) {
  return class extends Component {
    constructor(props) {
      super()
      this.props = props
      this.onPetitionSucceedBound = this.onPetitionSucceed.bind(this)
    }

    componentDidMount() {
      // from Local Storage
      this.changeUserData(getPetitionUserData(this.props["user-data-preload"]));

      if (window.jQuery && typeof(window.jQuery().on) === 'function') {
        window.jQuery(document).on('petition:didSucceed', this.onPetitionSucceedBound)
      }
      document.addEventListener('petition', this.onPetitionSucceedBound, {passive: true});
    }

    componentWillUnmount() {
      if (window.jQuery && typeof(window.jQuery().on) === 'function') {
        window.jQuery(document).off('petition:didSucceed', this.onPetitionSucceedBound)
      }
      document.removeEventListener('petition', this.onPetitionSucceedBound, {passive: true});
    }

    render() {
      return <WrappedComponent {...this.props} />
    }

    async onPetitionSucceed(e, event_data = null) {
      if (event_data && event_data.postData) {
        this.changeUserData(event_data.postData);
      }
      else if (e.detail && e.detail.type && e.detail.type === "finished" && e.detail.status && e.detail.status === "success") {
        this.changeUserData(e.detail.postData);
      }
    }

    // https://matthiashager.com/converting-snake-case-to-camel-case-object-keys-with-javascript
    object_snakelize(o) {
      const snakelize = (key) => key ? key.split(/(?=[A-Z])/).join('_').toLowerCase() : key;
      if (typeof o === 'object') {
        const n = {};
        Object.keys(o).forEach(k => { n[snakelize(k)] = this.object_snakelize(o[k]); });
        return n;
      } else if (Array.isArray(o)) {
        return o.map(i => this.object_snakelize(i));
      }
      return o;
    }

    changeUserData(data = {}) {
      if (!data) {
        return;
      }
      if ('firstName' in data || 'lastName' in data) {
        data = this.object_snakelize(data);
      }

      const { first_name, last_name, email, postal_code = ""} = data;
      if (first_name || last_name || email) {
        console.log(`[msender] change user data(${first_name}, ${last_name}, ${email}, ${postal_code})`);
        this.props.didGetPetitionData(first_name, last_name, email, postal_code);
      }
    }
  }
};
