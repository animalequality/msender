export default function sendEvent(name, data) {
  console.log(`[msender] sends event ${name}`, data);
  document.dispatchEvent(new CustomEvent('msender', { detail: { name, ...data } }));
}
