export default async (setIn, campaignEmailServiceUrl, setIsLoading) => {
    try{
      setIsLoading(true);
      const response = await fetch(campaignEmailServiceUrl)
      if(!response.ok) {
        throw new Error (`Request to ${response.url} failed with status ${response.status} ${response.statusText} `);
      }
      const data = await response.json();
      const emailMsg = data[0];
      const hasValidFields = typeof(emailMsg.body) == "string" && !!emailMsg.body && typeof(emailMsg.subject) == "string" && !!emailMsg.subject;
      if (!hasValidFields) {
        throw new Error (`Received email message data from ${response.url} was invalid. Inspect data: ${JSON.stringify(data)}`)
      };
      // await necessary otherwise async race condition overwrites message_text with default with next setIn() call
      await setIn(["message_text"], data[0].body);
      setIn(["message_subject"], data[0].subject);
    } catch (e) {
      console.warn(`Error handling random campaign email. ${e}`);
    } finally {
      setIsLoading(false)
    }
  }

