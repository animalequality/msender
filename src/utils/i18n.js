const DEFAULT_LANGUAGE = 'en';

import fr from  '../translations/fr.json';
import en from  '../translations/en.json';
import de from  '../translations/de.json';
import es from  '../translations/es.json';
import it from  '../translations/it.json';
import esmx from  '../translations/es-MX.json';
import ptbr from  '../translations/pt-BR.json';

const localeData = { fr, en, de, es, it, esmx, ptbr };

function translationMerger(locale, base, override) {
    var o = base[locale];

    if (!override) {
        return o;
    }
    if (typeof override[locale] === 'object') {
        Object.assign(o, override[locale]);
    }

    if (typeof override['*'] === 'object') {
        Object.assign(o, override['*']);
    }

    return o;
}

function localeGuesser(value = null) {
    // Validate parameter and return it if that locale is available for translation
    if (value && Object.keys(localeData).indexOf(value)) {
        return value;
    }

    // No browser => english
    if (typeof window === 'undefined') {
        return DEFAULT_LANGUAGE;
    }

    // if there's a valid hash to indicate the language, use it
    // e.g. #msender-locale=fr-FR
    var m;
    m = window.location.hash.match(/^#msender-locale=(fr|en|de)/i);
    if (m) {
        return m[1].toLowerCase();
    }

    let browser_language = window.navigator.language;
    if ((m = browser_language.match(/^(fr|en|de|es|es-MX|pt-BR|it)/i))) {
        return m[1].toLowerCase().replace('-', '');
    }

    return DEFAULT_LANGUAGE;
};



export { localeData, localeGuesser, translationMerger };
