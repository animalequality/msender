import React, { Component } from 'react'

import { MsenderContainer } from './components/msender'

import sendEvent from './utils/MessageBus';

const renderContainer = (domElement, props) => {
  if (typeof domElement === 'string') {
    domElement = document.querySelector(domElement)
  }
  React.render(<MsenderContainer {...props} />, domElement)
}

if (typeof window !== "undefined") {
    window.msender = {
        renderContainer
    }

    // dispatch our custom event
    const event = new Event('msenderReady')
    window.dispatchEvent(event)
    sendEvent('ready');
}
