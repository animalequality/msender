import React from 'react'
import {useState, useEffect} from 'preact/hooks'
import { Text, withText } from 'preact-i18n'

import style from './style.scss'

import ValueField from '../value-field'
import { FILTER_RECIPIENT_NONE } from '../../models/msender'
import BlocksShuffle from '../blocks-shuffle'
import ButtonContainer from '../button-container'
import { MESSENGER_MODE_NONE } from '../../models/messenger'
import { Fragment } from 'preact'
import handleLoadCampaignEmail from '../../utils/handleLoadCampaignEmail'

const MessagePreview = withText('preview_reload_title')((props) => {
  const { msender, setIn, preview_reload_title } = props
  const [loadingPreview, setLoadingPreview] = useState(props.loadingPreview)

  useEffect(() => {
    if (props.loadingPreview !== loadingPreview) {
      setLoadingPreview(props.loadingPreview);
    }
  }, [props.loadingPreview]);

  return (
    <div className={style.message_preview}>
      <div className={style.preview_title}>
        <Text id='title_preview' />
        {
          msender.isCampaignEmailServiceAvailable() &&
          <div title={preview_reload_title} className={style.preview_reload} onClick={() => handleLoadCampaignEmail(setIn, msender.getCampaignEmailServiceUrl(), (isLoading) => setLoadingPreview(isLoading))}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
              <path d="M463.5 224H472c13.3 0 24-10.7 24-24V72c0-9.7-5.8-18.5-14.8-22.2s-19.3-1.7-26.2 5.2L413.4 96.6c-87.6-86.5-228.7-86.2-315.8 1c-87.5 87.5-87.5 229.3 0 316.8s229.3 87.5 316.8 0c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0c-62.5 62.5-163.8 62.5-226.3 0s-62.5-163.8 0-226.3c62.2-62.2 162.7-62.5 225.3-1L327 183c-6.9 6.9-8.9 17.2-5.2 26.2s12.5 14.8 22.2 14.8H463.5z"/>
            </svg>
          </div>
        }
      </div>

    { loadingPreview ?
      <div className={style.blocks_shuffle_box} >
        <BlocksShuffle width={50} height={50}/>
      </div>
      :
      <React.Fragment>
        {msender.get('filter_recipient') !== FILTER_RECIPIENT_NONE && (
          <React.Fragment>
            <ValueField
              renderLabelText={() => (
                <Text id="label_value_send_to" />
              )}
              value={msender.getToEmailsString()}
            />
            <hr className={style.message_hr} />
          </React.Fragment>
        )}
        <ValueField
          renderLabelText={() => (
            <Text id="label_value_subject" />
          )}
          value={msender.getSubject()}
        />
        <hr className={style.message_hr} />
        <ValueField
          renderLabelText={() => (
            <Fragment>
              <Text id="label_value_message" /> <Text id="label_value_message_hint" />
            </Fragment>
          )}
          value={msender.getMessage()}
          multiline={true}
        />
      </React.Fragment>
    }

    {
      msender.get('messenger').getMode() !== MESSENGER_MODE_NONE &&
      !msender.disable_submit_button_preview &&
      <ButtonContainer msender={msender} mobileOnly={true} />
    }
    </div>
  )
})

export default MessagePreview
