import React from 'react'
import MicroModal from 'micromodal';

import style from './style.scss';

const ModalTrigger = ({ id, children }) => {
  const handleClick = () => {
    MicroModal.show(`modal-${id}`);
  };

  return (
    <div onClick={handleClick}>
      {children}
    </div>
  );
};

const Modal = ({id, title, children}) => {
  return <div className={style.msender_modal} id={`modal-${id}`} aria-hidden="true">
    <div className={style.msender_modal_overlay} tabindex="-1" data-micromodal-close>
      <div className={style.msender_modal_container} role="dialog" aria-modal="true" aria-labelledby={`modal-${id}-title`}>
        <button className={style.msender_modal_close} aria-label="Close modal" data-micromodal-close></button>
        <div className={style.msender_modal_title}>
          {title}
        </div>
        <div className={style.msender_modal_content}>
          {children}
        </div>
      </div>
    </div>
  </div>
}

export {Modal, ModalTrigger}
