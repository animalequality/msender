import React from 'react'
import style from './style.scss'

const Input = props => {
  return (
    <input className={style.input} {...props} />
  )
}
Input.defaultProps = { type: 'text' }

export default Input
