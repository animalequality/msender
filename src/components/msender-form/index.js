import React, { Component } from 'react'
import { Text, MarkupText, Localizer } from 'preact-i18n';

import style from './style.scss'

import Step from '../step'
import InputLabel from '../input-label'
import SelectLabel from '../select-label'
import Checkbox from '../checkbox'
import CopyAdvice from '../copy-advice'
import ButtonContainer from '../button-container'
import Help from '../help'

import { MESSENGER_MODE_NONE, MESSENGER_MODE_COPY } from '../../models/messenger'
import detectEmailMessenger from '../../utils/detectEmailMessenger'
import isEmailValid from '../../utils/isEmailValid'
import sendEvent from '../../utils/MessageBus'

class MsenderFormStepInfo extends Component {
  constructor(props) {
    super()
    this.props = props
    this.onInputFirstName = this.onInputFirstName.bind(this)
    this.onInputLastName = this.onInputLastName.bind(this)
    this.onInputEmail = this.onInputEmail.bind(this)
    this.onChangeEmail = this.onChangeEmail.bind(this)
  }

  onInputFirstName(e) {
    const { setIn } = this.props
    setIn(['first_name'], e.target.value)
  }

  onInputLastName(e) {
    const { setIn } = this.props
    setIn(['last_name'], e.target.value)
  }

  onInputEmail(e) {
    const { setIn } = this.props
    setIn(['email'], e.target.value)
  }

  onChangeEmail(e) {
    const { msender, setIn } = this.props
    sendEvent('change', {field: 'email'});

    // ignore on mobile
    if (msender.get('is_mobile_or_tablet')) {
      return
    }

    // detect email messenger
    detectEmailMessenger(e.target.value).then(async messenger => {
      if (messenger) {
        await setIn(['email'], e.target.value)
        setIn(['messenger'], messenger)
        sendEvent('change', {field: 'messenger', value: messenger.get('identifier')});
      }
    })
  }

  render() {
    const { msender, number } = this.props
    return (
      <Step
        renderTitle={() => <Text id="step_my_infos" />}
        number={msender.disable_step_number ? null : number}
        tooltipTextId="tooltip_step_my_infos"
      >
        <InputLabel
          renderLabelText={() => <Text id="label_first_name" />}
          value={msender.get("first_name")}
          onInput={this.onInputFirstName}
          onChange={() => sendEvent("change", { field: "firstname" })}
        />
        <InputLabel
          renderLabelText={() => <Text id="label_last_name" />}
          value={msender.get("last_name")}
          onInput={this.onInputLastName}
          onChange={() => sendEvent("change", { field: "lastname" })}
        />
        <InputLabel
          renderLabelText={() => <Text id="label_email" />}
          type="email"
          value={msender.get("email")}
          onInput={this.onInputEmail}
          onChange={this.onChangeEmail}
        />
      </Step>
    );
  }
}

class MsenderFormStepDetails extends Component {
  constructor(props) {
    super()
    this.props = props
    this.onChangeDepartment = this.onChangeDepartment.bind(this)
    this.onChangeFilterRecipient = this.onChangeFilterRecipient.bind(this)
  }

  getStepName() {
    const { msender } = this.props
    let name = msender.get('step_two_title')
    if (!name) {
      name = 'Destinataires'
    }
    return name
  }

  onChangeDepartment(e) {
    const { msender, setIn } = this.props
    var department = msender.getDepartments().find(d => e.target.value === d.get('code'));
    setIn(['department'], department)
    sendEvent('change', {field: 'department', value: department.toJS()});
  }

  onChangeFilterRecipient(e) {
    const { msender, setIn } = this.props
    var recipient = msender.get('message_to').find(d => e.target.value === d.get('email'));
    setIn(['message_to_current'], recipient);
    sendEvent('change', {field: 'recipient-filter', value: recipient.toJS()});
  }

  renderSelectDepartment() {
    const { msender, enabled } = this.props
    if (!msender.enableDepartmentSelect()) {
      return null
    }
    return (
      <SelectLabel
        renderLabelText={() => (
          <Text id="label_department" />
        )}
        value={msender.getIn(['department', 'code'])}
        options={msender.getDepartments().map(d => d.getSelectOption()).toArray()}
        enabled={enabled}
        onChange={this.onChangeDepartment}
      />
    )
  }

  renderFilterRecipient() {
    const { msender, enabled } = this.props
    if (!msender.enableRecipientSelect()) {
      return null
    }
    return (
      <SelectLabel
        renderLabelText={() => (
          <Text id="label_recipient" />
        )}
        value={msender.getIn(['message_to_current', 'email'])}
        options={msender.get('message_to').map(d => d.getSelectOption()).toArray()}
        enabled={enabled}
        onChange={this.onChangeFilterRecipient}
      />
    )
  }

  render() {
    const { msender, number } = this.props
    return (
      <Step
        renderTitle={() => this.getStepName()}
        number={msender.disable_step_number ? null : number}
      >
        {this.renderSelectDepartment()}
        {this.renderFilterRecipient()}
      </Step>
    );
  }
}

class _MsenderFormStepSend extends Component {
  constructor(props) {
    super()
    this.props = props
    this.onChangeMessenger = this.onChangeMessenger.bind(this)
    this.onSend = this.onSend.bind(this)
  }

  onChangeMessenger(e) {
    const { msender, setIn } = this.props
    var value = msender.getMessengers().find(d => e.target.value === d.get('identifier'));
    setIn(['messenger'], value)
    sendEvent('change', {field: 'messenger', value: value.toJS()});
  }

  onSend() {
    const { msender, setIn, enabled } = this.props
    const sendButtonEnabled = enabled && !!msender.getIn(['messenger', 'identifier']);
    sendEvent('sent', {field: 'submit', value: sendButtonEnabled ? 'ok' : 'error', msender: msender.toJS()});

    this.props.setShowErrorMessage(!sendButtonEnabled);
  }

  renderSelectMessenger() {
    const { msender, intl } = this.props
    if (msender.get('is_mobile_or_tablet')) {
      return null
    }
    return (
        <SelectLabel
          renderLabelText={() => (
              <Text id="label_email_client" />
          )}
          value={msender.getIn(['messenger', 'identifier'])}
          options={msender.getMessengers().map(m => ({
              value: m.get('identifier'),
              text: m.get('name')
          })).toArray()}
          onChange={this.onChangeMessenger}
          i18n={true}
        />
    )
  }

  renderCopyAdvice() {
    const { msender } = this.props

    if (msender.get('messenger').getMode() === MESSENGER_MODE_COPY || msender.get('messenger').getMode() === MESSENGER_MODE_NONE) {
      return <CopyAdvice />
    }
    return null
  }

  renderSendButton() {
    const { msender, enabled } = this.props
    if (msender.get('messenger').getMode() === MESSENGER_MODE_NONE) {
      return null
    }
    const sendButtonEnabled = (enabled && !!msender.getIn(['messenger', 'identifier']))
    return (
      <ButtonContainer msender={msender}
                       enabled={sendButtonEnabled}
                       onClick={this.onSend}/>
    )
  }

  render() {
    const { msender, number } = this.props
    return (
      <Step
        renderTitle={() => <Text id="step_send" />}
        number={msender.disable_step_number ? null : number}
      >
        {this.renderSelectMessenger()}
        {this.renderCopyAdvice()}
        {this.renderSendButton()}
      </Step>
    );
  }
}

const MsenderFormStepSend = _MsenderFormStepSend;

export default class MsenderForm extends Component {
  state = {
    showErrorMessage: false,
    stepThreeEnabled: false
  };

  stepThreeEnabled() {
    return this.props.msender.isInfosValid() &&
      this.props.msender.isDepartmentValid() &&
      this.props.msender.isFilterRecipientValid()
  }

  componentDidUpdate(prevProps, prevState) {
    const stepThreeEnabled = this.stepThreeEnabled();

    if (prevState.stepThreeEnabled !== stepThreeEnabled) {
      this.setState({
        showErrorMessage: this.state.showErrorMessage && !stepThreeEnabled,
        stepThreeEnabled
      })
    }
  }

  render() {
    const { msender, setIn } = this.props

    // step numbers
    const infoStep = 1
    let detailsStep = null
    let sendStep = 2
    if (msender.enableDepartmentSelect() || msender.enableRecipientSelect()) {
      detailsStep = 2
      sendStep = 3
    }

    // step activation
    let stepTwoEnabled = msender.isInfosValid()

    return (
      <div className={style.msender_form}>
        <Help msender={msender} />

        <MsenderFormStepInfo {...{msender,setIn}}
                             number={1} />
        {detailsStep !== null ?
          (
            <MsenderFormStepDetails {...{msender, setIn}}
                                    number={detailsStep}
                                    enabled={stepTwoEnabled} />
          ) : null
        }
        <MsenderFormStepSend {...{msender,setIn}}
                            number={sendStep}
                            enabled={this.stepThreeEnabled()}
                            setShowErrorMessage={(show) => this.setState({showErrorMessage: show})}/>
        {
          !msender.disable_hint_mobile &&
          <div className={style.message_hint_mobile}>
            <Text id='message_hint_mobile'></Text>
          </div>
        }
        {
            this.state.showErrorMessage &&
            <p className={style.error_submit}>
              <Text id="error_message_submit" />
            </p>
        }
      </div>
    )
  }
}
