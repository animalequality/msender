import React, { Component } from 'react'
import style from './style.scss'

import CopyButton from '../button-copy'

class ValueField extends Component {
  constructor(props) {
    super()
    this.props = props
    this.state = {
      inputId: Math.round(Math.random()*1e9).toString()
    }
    this.inputRef = null
    this.selectContentBound = this.selectContent.bind(this)
  }

  render() {
    const { props } = this
    const { inputId } = this.state
    return (
      <div className={style.value_field}>
        <label htmlFor={inputId} className={style.label} onClick={this.selectContentBound}>
          <span>{props.renderLabelText()}</span>
          <CopyButton value={props.value} />
        </label>

          {props.multiline ?
           ( <textarea id={inputId}
                       readonly
                       className={style.input_multiline}
                       value={props.value}
                       ref={(ref) => this.inputRef = ref}
                       onFocus={this.selectContentBound}
             /> ) :
           ( <input id={inputId}
                    type="text"
                    readonly
                    className={style.input}
                    value={props.value}
                    ref={(ref) => this.inputRef = ref}
                    onFocus={this.selectContentBound}
             /> )
          }
      </div>
    )
  }

  selectContent() {
    if (this.inputRef) {
      this.inputRef.setSelectionRange(0, this.inputRef.value.length)
    }
  }
}

export default ValueField
