import React from 'react'
import { Text } from 'preact-i18n';
import { Modal, ModalTrigger } from '../modal';

import style from './style.scss';

const Help = ({msender}) => {
  return <div className={style.help}>
    <ModalTrigger id='help'>
      <a>
        <Text id='help_label' />
      </a>
    </ModalTrigger>

    <Modal id='help' title={<Text id='help_label' />}>
      <p>
        <Text id='help_text' />
      </p>
      {
        msender.isCampaignEmailServiceAvailable() &&
        <p>
          <Text id='help_text_campaign' />
        </p>
      }
      <ol>
        <li>
          <Text id='help_step_my_infos' />
        </li>
        {
          msender.getSelectDepartmentMode() &&
          <li>
            <Text id='help_step_filter' />
          </li>
        }
        <li>
          <Text id='help_step_send' />
        </li>
        <li>
          <Text id='help_send' />
        </li>
      </ol>
    </Modal>
  </div>
}

export default Help
