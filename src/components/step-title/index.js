import React from 'react'
import style from './style.scss'

const StepTitle = props => {
  return (
    <div className={style.step_title}>
      {props.number && <span className={style.step_number}>{props.number}</span>}
      <span className={style.step_text}>{props.children}</span>
    </div>
  );
}

export default StepTitle
