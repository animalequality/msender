import React from 'react'

import Input from '../input'
import LabelContainer from '../label-container'

const InputLabel = props => {
    var { renderLabelText, enabled, ...others } = props;
    return (
        <LabelContainer renderLabelText={renderLabelText} enabled={enabled}>
          <Input {...others} />
        </LabelContainer>
    );
}

export default InputLabel
