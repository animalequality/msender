import React from 'react'
import style from './style.scss'
import { Text } from 'preact-i18n';

const Select = props => {
  const enabled = ('undefined' === typeof props.enabled || props.enabled)
  const usei18n = props.i18n || false;
  return (
    <select className={style.select} value={props.value} onChange={props.onChange} disabled={!enabled}>
      {props.options.map(val => {
          return usei18n
              ? <option key={val.value} value={val.value}><Text id={val.text}>{val.text}</Text></option>
              : <option key={val.value} value={val.value}>{val.text}</option>
      })}
    </select>
  )
}

export default Select
