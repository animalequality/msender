import React, { Component} from 'react'

import style from './style.scss'

import MsenderForm from '../msender-form'
import MessagePreview from '../message-preview'

import { msenderFromProps } from '../../models/msender'
import detectEmailMessenger from '../../utils/detectEmailMessenger'
import withPetitionBindings from '../../utils/withPetitionBindings'
import handleLoadCampaignEmail from '../../utils/handleLoadCampaignEmail'
import { localeData, localeGuesser, translationMerger } from '../../utils/i18n'

import { IntlProvider } from 'preact-i18n';

const MSenderUI = withPetitionBindings((props) => {
  const { msender } = props
  return (
    <div className={style.msender}>
      <MsenderForm {...props} />
      <MessagePreview {...props} />
    </div>
  )
})

class MsenderContainer extends Component {
  constructor(props) {
    super()

    const msender = msenderFromProps(props)

    this.state = {
      msender: msender,
      loadingPreview: msender.isCampaignEmailServiceAvailable()
    }
    this.setInBound = this.setIn.bind(this)
    this.didGetPetitionDataBound = this.didGetPetitionData.bind(this);
  }

  componentWillMount() {
    this.changeLanguage();
  }

  componentDidMount() {
    if (this.state.msender.isCampaignEmailServiceAvailable()) {
      handleLoadCampaignEmail(this.setInBound, this.state.msender.getCampaignEmailServiceUrl(), (isLoading) => this.setState({loadingPreview: isLoading}));
    }
  }

  render() {
    const { msender, loadingPreview } = this.state
    return (
        <IntlProvider definition={msender.translations}>
          <MSenderUI
            {...this.props}
            loadingPreview={this.state.loadingPreview}
            msender={msender}
            setIn={this.setInBound}
            didGetPetitionData={this.didGetPetitionDataBound}
          />
        </IntlProvider>
    )
  }

  changeLanguage = (lang = null) => {
      var l = localeGuesser(lang);
      console.log(`Switching locale to ${l}`);
      this.setIn(['locale'], l);
      this.setIn(['translations'], translationMerger(l, localeData, this.props.translations));
  };

  setIn(keys, value) {
    const { msender } = this.state
    this.setState({
      msender: msender.setIn(keys, value)
    })
  }

  didGetPetitionData(first_name, last_name, email, postal_code = "") {
    const { msender } = this.state
    const departments = msender.getDepartments()
    if ((!msender.get('first_name') || msender.get('first_name').length === 0) &&
        (!msender.get('last_name') || msender.get('last_name').length === 0)) {
      this.setIn(['first_name'], first_name)
      this.setIn(['last_name'], last_name)
      this.setIn(['email'], email)
      this.setIn(['department'], departments.find(d => postal_code.substr(0, 2).toString() === d.get('code')))

      // ignore on mobile
      if (!msender.get('is_mobile_or_tablet')) {
        detectEmailMessenger(email).then(messenger => {
          if (messenger) {
            this.setIn(['messenger'], messenger)
          }
        })
      }
    }
  }
};

export { MsenderContainer, MSenderUI }
