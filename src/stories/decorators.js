import React from 'react'

import { IntlProvider } from 'preact-i18n';
import defaultDefinition from '../translations/en.json';

const style = {
  backgroundColor: '#EB9339',
  padding: '20px',
  borderRadius: '10px'
}

export const StoryDecorator = (props) => {
  const s = Object.assign({}, style, props.style)
  return (
    <div style={s}>
      {props.children}
    </div>
  )
}

export const decoratorFn = props => story => (
  <StoryDecorator {...props}>
    {story()}
  </StoryDecorator>
)

export const decoratorIntlFn = props => story => {
  return (
    <IntlProvider definition={defaultDefinition}>
      {story()}
    </IntlProvider>
  )
}
