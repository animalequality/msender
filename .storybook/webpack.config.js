const path = require("path");
const webpack  = require('webpack');

// const defaults = require('@storybook/react/dist/server/config/defaults/webpack.config');
module.exports = async ({ config, mode }) => {
    config.resolve.alias['react'] = 'preact/compat';
    config.resolve.alias['react-dom'] = 'preact/compat';

    config.plugins.push(new webpack.ProvidePlugin({
        Component: ['preact', 'Component'],
        React: ['preact/compat']
    }));

    config.module.rules.push({
        test: /\.s[ac]ss$/,
        loaders: [
            'style-loader?sourceMap',
            {
                loader: 'css-loader',
                options: {
                    modules: true,
                    importLoaders: 1,
                }
            },
            'sass-loader?sourceMap',
        ],
        include: path.resolve(__dirname, "../"),
    });

    return config;
};
