import { configure } from '@storybook/react';

const req = require.context('../src/stories', true, /\.js$/);
configure(req, module)
